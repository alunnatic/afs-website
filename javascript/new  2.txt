
<!-- TWO STEPS TO INSTALL FILE OPEN:

  1.  Copy the coding into the HEAD of your HTML document
  2.  Add the last code into the BODY of your HTML document  -->

<!-- STEP ONE: Paste this code into the HEAD of your HTML document  -->

<HEAD>

<SCRIPT LANGUAGE="JavaScript">
<!-- Original:  Hoop Hooper (hooper119@aol.com) -->
<!-- Web Site:  http://www.angelfire.com/pa3/muddas -->
<!-- Modified:  Larry McClurg (larry@honeybells.com) -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://www.javascriptsource.com -->

<!-- Begin
// Thank you Larry McClurg for the cross-browser fix
function whatFile() {
window.location= 'file:///' + document.form1.cmuds.value;
}
//  End -->
</script>

</HEAD>

<!-- STEP TWO: Copy this code into the BODY of your HTML document  -->

<BODY>

<form name=form1>
<input type=file name="cmuds">
<input type=button onClick="whatFile()" value="Open File">
</form>

<p><center>
<font face="arial, helvetica" size"-2">Free JavaScripts provided<br>
by <a href="http://javascriptsource.com">The JavaScript Source</a></font>
</center><p>

<!-- Script Size:  1.09 KB -->